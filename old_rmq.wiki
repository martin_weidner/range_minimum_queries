[[Datei:01 array.svg|links|Range Minimum Query auf ein Feld]]
Range Minimum Queries (RMQs) adressieren innerhalb der Informatik das Problem eine Anfrage nach dem kleinsten Element innerhalb eines spezifizierten Bereichs eines Arrays zu beantworten. Eine effiziente Beantwortung dieser Anfrage hat Relevanz, da sie in einigen weiteren Algorithmen der Informatik benötigt wird, wie z.B. in [[String-Matching-Algorithmen|String Matching Algorithmen]], [[http://en.wikipedia.org/wiki/Lowest_common_ancestor|LCA]], [[http://de.wikipedia.org/wiki/Suffixbaum|Suffix Bäume]].

==Definition==
Gegeben sei ein Array <math>A</math> der Länge <math>1..n</math> mit Elementen eines Universums totaler Ordnung, dann liefere <math>RMQ_A(i, j) = \arg\min_{i\le k \le j} A[k]</math> (<math>1 \le i \le j \le n</math>) die Position des kleinsten Elements innerhalb des angegebenen Intervalls. 
<br />

Im Folgenden wird Schrittweise gezeigt, wie das gegebene Problem mittels geschickter Vorberechnung in <math>\langle O(n), O(1) \rangle</math> (linearer Platzbedarf/Vorberechnungszeit, konstante Anfragezeit) gelöst werden kann.

==Triviale Bearbeitung==
Es existieren zwei triviale Lösungen zur Problembearbeitung, die entweder platz- oder zeitineffizient sind.
# Lineares Scannen: Durch ''scannen'' von <math>A[i, j]</math> wird für jede Query eine ''lineare'' Anfragezeit in <math>O(n)</math> worst-case und ein Platzbedarf von <math>O(n)</math> erreicht. 
# Vorberechnung einer Lookup Tabelle: Naiv wird eine Tabelle vorberechnet, die die Antworten auf alle möglichen Range Minimum Anfragen speichert. Somit wird eine ''konstante'' Anfragezeit in <math>O(1)</math> erreicht, wobei <math>\tbinom{n}{2}</math> Kombinationen <math>O(n^2)</math> Platz benötigt wird.

Es wird allerdings davon ausgegangen, dass es sich bei <math>A</math> um ein statisches Array handelt und die Anfragen zufällig (on-line) gestellt, wodurch eine geschickte Vorberechnung und die Konstruktion einer geeigneten Datenstruktur die Anfragezeit deutlich reduzieren kann. Hierbei wird eine nahezu konstante Anfragezeit mit linearem Platzverbrauch angestrebt.

==Effiziente Konstruktion==

===Logarithmischer Platzbedarf===

===Linearer Platzbedarf===

==Anwendung==

==Referenzen==
* Fischer, Johannes; Heun, Volker. "Space-efficient preprocessing schemes for range minimum queries on static arrays". SIAM J. Comput., 40(2), pp. 465–492, April 2011. ISSN 0097-5397. doi: 10.1137/090779759. http://dx.doi.org/10.1137/090779759.
* Fischer, Johannes. "Range minimum queries: Simple and optimal, at last!", 2008.
* Fischer, Johannes; Heun, Volker. "A New Succinct Representation of RMQ-Information and Improvements in the Enhanced Suffix Array.", Proceedings of the International Symposium on Combinatorics, Algorithms, Probabilistic and Experimental Methodologies, Lecture Notes in Computer Science, 4614, Springer-Verlag, pp. 459–470, 2007, [http://dx.doi.org/10.1007%2F978-3-540-74450-4_41 doi:10.1007/978-3-540-74450-4_41], ISBN 978-3-540-74449-8.
* Vishkin, Uzil; Berkman, Omer. Recursive star-tree parallel data-structure, 1990.

