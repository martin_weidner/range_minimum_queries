%
%  Range Minimum Queries
%
%  Created by Martin Weidner on 2012-08-16.
%  Copyright (c) 2012 . All rights reserved.
%
\documentclass[]{article}
\usepackage{ngerman}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{boxedminipage}
\usepackage{listings}
\usepackage{ifpdf}
\usepackage{hyperref}
\usepackage{nicefrac}
\usepackage{natbib}
\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi
\title{Range Minimum Queries}
\author{Martin Weidner}

\date{\today}

\begin{document}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif, .png}
\else
\DeclareGraphicsExtensions{.eps, .jpg, .png}
\fi

\maketitle
\tableofcontents
\begin{abstract}
	Range Minimum Queries (RMQs) adressieren innerhalb der Informatik das Problem, eine Anfrage nach dem kleinsten Element innerhalb eines spezifizierten Bereichs eines Arrays zu beantworten. Eine effiziente Beantwortung hat Relevanz für weitere Probleme der Informatik, wie z.B. für die Textindizierung und -komrpession, Flussgraphen etc. \citep{Fischer08rangeminimum, berkman1989recursive}
\end{abstract}

\section{Definition}
Sei $A$ ein Array der Länge $1..n$ mit Elementen eines Universums totaler Ordnung, dann liefere $\operatorname{RMQ}_A(l, r) = \arg\min_{l\le k \le r} A[k]$ ($1 \le l \le r \le n$) die Position des kleinsten Elements innerhalb des angegebenen Intervalls \citep{Fischer:2011:SPS:2078866.2078875}.\\ Abbildung~\ref{fig:01-rmq} veranschaulicht die Anfrage auf eine Beispielsequenz.
\begin{figure}[!ht]
		\centering  
		\includegraphics[width=0.3\textwidth]{Grafiken/01-array.pdf}
		\caption{Range Minimum Anfrage auf Integer-Array}
		\label{fig:01-rmq}
	\end{figure}

\section{Triviale Bearbeitung}
Es existieren zwei triviale Lösungen zur Problembearbeitung, die entweder platz- oder zeitineffizient sind. \citep{Fischer:2011:SPS:2078866.2078875}
\begin{itemize}
	\item \textbf{Lineares Scannen:} Durch \emph{scannen} von $A[l, r]$ wird für jede Query eine \emph{lineare} Anfragezeit in $\mathcal{O}(n)$ worst-case und ein Platzbedarf von $\mathcal{O}(n)$ erreicht.
	\item \textbf{Vorberechnung einer Lookup Tabelle:} Naiv wird eine Tabelle vorberechnet, die die Antworten auf alle möglichen Range Minimum Anfragen speichert. Somit wird eine \emph{konstante} Anfragezeit in $\mathcal{O}(1)$ erreicht, wobei $\tbinom{n}{2}$ Kombinationen $\mathcal{O}(n^2)$ Platz benötigen.
\end{itemize}\par
Angenommen wird allerdings, dass es sich bei $A$ um ein statisches Array handelt und die Anfragen on-line gestellt werden, wodurch eine geschickte Vorberechnung und die Konstruktion einer geeigneten Datenstruktur die Anfragezeit deutlich reduzieren kann. Hierbei wird eine nahezu \emph{konstante} Anfragezeit mit \emph{linearem} Platzverbrauch angestrebt.

\section{Effiziente Konstruktion}
Im Folgenden wird Schrittweise dargestellt, wie das gegebene Problem mittels geschickter Vorberechnung auf eine Komplexität von $\langle \mathcal{O}(n), \mathcal{O}(1) \rangle$ (linearer Platzbedarf/Vorberechnungszeit, konstante Anfragezeit) reduziert werden kann.

\subsection{Logarithmischer Platzbedarf}
\label{sub:red_log}
\citet{Bender200575}
\begin{figure}[!ht]     
		\centering  
		\includegraphics[width=0.5\textwidth]{Grafiken/02-logn.pdf}
		\caption{Zwei Teilanfragen für die RMQ}  
		\label{fig:02-logn}
	\end{figure}
Um $\langle\mathcal{O}(n \log n), \mathcal{O}(1)\rangle$ zu erreichen, werden alle Anfragen vorberechnet, deren Länge $2^x : x=\mathcal{O}(\log n)$ umfassen. Aufgrund dessen wird die tatsächliche RMQ in maximal zwei Teilanfragen zerlegt, deren Längen 2er-Potenz sind und sich ggf. überlappen. Hierbei wird die größtmögliche 2er-Potenz $h$ verwendet, wobei $h = \lfloor \log_2{(r-l-1)} \rfloor$ ist. Aufgrund der Vorberechnung kann das Ergebnis über das Intervall von $2^h$ in konstanter Zeit bestimmt werden. Abbildung~\ref{fig:02-logn} verbildlicht das Vorgehen und zeigt beispielhaft, wie die RMQ (Intervall = rote Linie) in zwei RMQs (graue Linien) zerlegt wird.\\
Sei $M$ die Tabelle mit den vorberechneten Antworten. Der Algorithmus umfasst insgesamt drei Schritte, um das Ergebnis einer RMQ zu bestimmen:
\begin{enumerate}
	\item $\operatorname{RMQ}_A(l, l+2^h-1) = m_1 = M[l][h]$
	\item $\operatorname{RMQ}_A(r-2^h+1, r) = m_2 = M[r-2^h+1][h]$
	\item $\operatorname{RMQ}_A(l, r) = \min{A[m_1], A[m_2]}$
\end{enumerate}

\noindent\textbf{Analyse}\\
Aufgrund der Vorberechnung kann eine RMQ in $\mathcal{O}(1)$ beantwortet werden. Die zusätzliche Datenstruktur entält für jede Position innerhalb des Arrays maximal $\log n$ vorberechnete Minima, weil es im worst-case $\log n$ Zweierpotenzen von der ersten Arrayposition bis zum Ende gibt. Hierdurch reduziert sich der Platzbedarf von $\mathcal{O}(n^2)$ auf $\mathcal{O}(n \log n)$.\\
Die Vorberechnung benötigt mittels dynamischer Programmierung eine Laufzeit von $\mathcal{O}(n \log n)$ Schritten, wobei folgende Rekurrenz gelöst wird: $M[i][h] = \arg\min{\{A[M[i][h-1]], A[M[i+2^{h-1}][h-1]]\}}$ (siehe Abbildung~\ref{fig:03-vorberechnung}), um die entsprechende Tabelle $M$ zu berechnen.
\begin{figure}[!ht]     
		\centering  
		\includegraphics[width=0.4\textwidth]{Grafiken/03-recurrence.pdf}
		\caption{Vorberechnung}  
		\label{fig:03-vorberechnung}
	\end{figure}

\subsection{Linearer Platzbedarf}
\begin{figure}[!ht]     
		\centering  
		\includegraphics[width=0.5\textwidth]{Grafiken/04-blockbildung.pdf}
		\caption{Blockbildung und Speicherung der Minima}  
		\label{fig:04-blockbildung}
	\end{figure}
Im zweiten Optimierungsschritt wird der Platzbedarf auf $\mathcal{O}(n)$ reduziert. Dabei wird das Eingabearray $A$ in Blöcke der Länge $s=\frac{\log n}{2+\epsilon}$ zerlegt. O.B.d.A. sei $\epsilon = 2$. Visuell ist die Blockbildung in Abbildung~\ref{fig:04-blockbildung} dargestellt, wobei $A$ hier beispielhaft in vier Blöcke zerlegt wurde. Die RMQ kann nun in max. drei Teile zerlegt werden:
\begin{itemize}
	\item Eine Anfrage, die vollständige Blöcke umfasst ($3$).
	\item Zwei Anfragen, die jeweils einen Teil eines Blocks [\emph{in-Block}] betreffen ($1$, $2$).
\end{itemize}
\subsubsection{Anfrage über vollständige Blöcke}
Die RMQ, die an Blockgrenzen abschließt (also \emph{vollständige Blöcke} umfasst), kann mittels der Konstruktion aus Abschnitt~\ref{sub:red_log} in $\mathcal{O}(1)$ beantwortet werden. Man speichere das Minimum eines jeden Blockes in einer Liste $D$ (Abbildung~\ref{fig:04-blockbildung}, zudem sei $m = |D| = \frac{n}{s}$ . Hierauf wird die bekannte Konstruktion angewandt, woduch sich ein Platzbedarf von $\mathcal{O}(m \log m) =  \mathcal{O}(\frac{n}{\log n} \log \frac{n}{\log n}) = \mathcal{O}(n)$ ergibt. Zusammenfassend kann gesagt werden, dass für diesen Fall eine RMQ in \emph{konstanter} Zeit berechnet werden kann, wobei die Hilfsdatenstruktur \emph{linear} viel Platz benötigt.
\subsubsection{Anfragen auf Blockteile}
Jedem Block $B_i$ wird ein \emph{Kartesischer Baum}\footnote{Siehe \url{http://en.wikipedia.org/wiki/Cartesian_tree} für weitere Informationen.} zugeordnet. Ein \emph{Kartesischer Baum} wird nach folgender, rekursiver Vorschrift konstruiert \citet{Fischer:2011:SPS:2078866.2078875} und ist beispielhaft in Abbildung~\ref{fig:05-cartesian} zu sehen.
\begin{enumerate}
	\item Wurzel: Postition des Minimums in $B_i[1,s] = m$
	\item Linkes Kind: Kartesischer Baum von $B_i[1, m-1]$
	\item Rechtes Kind: Kartesischer Baum von $B_i[m+1, s]$
\end{enumerate}
\begin{figure}[!ht]     
 		\centering  
 		\includegraphics[width=0.5\textwidth]{Grafiken/05-cartesian.pdf}
 		\caption{Beispiel für Karteische Bäume}  
 		\label{fig:05-cartesian}
 	\end{figure}
\textbf{Lemma: } Falls die Kartesischen Bäume zweier Arrays $B_i, B_j; i\ne j$ die gleiche Struktur aufweisen, so gilt $\forall l,r; 1 \le l \le r \le n : \operatorname{RMQ}_{B_i}(l, r) = \operatorname{RMQ}_{B_j}(l, r)$.\\
Der interessierte Leser kann den Beweis des Lemmas in \cite[S.472]{Fischer:2011:SPS:2078866.2078875} nachlesen.
\par 
Aufgrund des Lemmas ergibt sich eine Reduktion für in-Block RMQs, da nicht jeder Block einzeln vorberechnet wird, sondern nur die Antworten für alle möglichen Kartesischen Bäume über $s$ Elementen ausreichen, denn jeder Block lässt sich in Linearzeit \citep{Fischer:2011:SPS:2078866.2078875} auf einen Kartesischen Baum abbilden, da die Baumkonstruktion armotisiert in $\mathcal{O}(n)$ liegt. Die Anzahl der Bäume entspricht der Catalan-Zahl $C_s = \frac{1}{s+1} \tbinom{s}{2s}$. Die \emph{Bitmuster} der Bäume dienen als \emph{Index} für die vorberechnete in-Block Tabelle $P[1,C_s][1,s][1,s]$. Ein Kartesischer Baum wird hierbei durch seine \emph{Level-order unary degree sequence} (LOUDS) \citep{jacobson1989space} eindeutig repräsentiert. \\Insgesamt ergibt sich daraus ein Platzbedarf von $|P| = \mathcal{O}(2^{2s} * s * s) = \mathcal{O}(\sqrt{n}\log^2 n) = o(n)$. \\ \par
Wie gezeigt wurde, kann das Problem auf \emph{linearen Platzbedarf} und \emph{konstante Anfragezeit} reduziert werden, indem die ursprüngliche RMQ in drei Teilanfragen zerlegt wird. Das Minimum über vollständig umfasste Blöcke wird unter Zuhilfenahme des Schemas aus Abschnitt~\ref{sub:red_log} berechnet, für in-Block Anfragen werden Blöcke auf Kartesische Bäume abgebildet, deren Ergebnisse vollständig vorberechnet nicht mehr als linear viel Platz bzgl. der Eingabe benötigen.
% \\
% \begin{itemize}
%  	\item Ansatz: $n=1$: $C(B_i) = C(B_j)$, da nur ein Konten im Baum vorhanden, somit ist $\operatorname{RMQ}(1,1) = 1$.
%  	\item Schritt: $n \rightarrow n+1$: Sei $v$ die Wurzel beider Bäume mit dem Label $\mu$. Da beide Bäume strukturgleich sind, gilt zudem $\arg\min_{1\le k\le n} B_i[k] = \mu = \arg\min_{1\le k\le n} B_j[k]$.
%  \end{itemize} 

\section{Anwendungen}
\subsection{Lowest Common Ancestor}
\begin{figure}[!ht]     
		\centering  
		\includegraphics[width=0.4\textwidth]{Grafiken/06-trafo.pdf}
		\caption{Reduktion von LCA auf RMQ}
		\label{fig:06-trafo}
	\end{figure}
Eine LCA-Anfrage bzgl. eines Baumes $S=(V, E)$ und zwei Knoten $v, w \in V$ liefert entweder $v$ (bzw. $w$), falls sich dieser auf dem Pfad von der Wurzel zu $w$ (bzw. $v$) befindet, oder denjenigen Knoten $u$, an dem der gemeinsame Pfad zu $u$ und $v$ endet.\par
Wie erstmals von \citet{gabow1984scaling} beschrieben wurde, kann das LCA Problem linear auf das RMQ Problem reduziert werden (und umgedreht, siehe hierzu \citet{bender2000lca}). Dies ist von Relevanz, da somit auch LCA-Anfragen in \emph{konstanter Zeit} und \emph{linearem Platzbedarf} bzgl. der Eingabe ($\langle \mathcal{O}(n), \mathcal{O}(1)\rangle$) gelöst werden können, wie es zum Beispiel in \citet{fischer2007new} dargestellt wird.\par
Die Reduktion beinhaltet einen \emph{in-Order Baumtraversal} ($\mathcal{O}(n) Zeit$) über den LCA-Baum $T$ zur Abbildung in ein Array $N$, wobei für jedes Element in $N$ eine entsprechende Traversalnummer in $D$ gespeichert wird, indem beim Abstieg um $1$ dekrementiert (bzw. beim Aufstieg inkrementiert) wird, siehe hierzu Abbildung~\ref{fig:06-trafo}. Das Array $N$ benötigt \emph{linear} viel Platz, weil die Kantenanzahl des Ursprungsbaumes durch dessen Knoten beschränkt ist. Für das Array $D$ kann nun die zuvor-beschriebene Vorberechnung für RMQs durchgeführt werden. Zur Lösung des LCA-Problems gilt nun: $\operatorname{LCA}_T(v, w) = N[\operatorname{RMQ}_D(p_v, p_w)], p_v \le p_w$, wobei $p_v, p_w$ die Position der Eingabeknoten innnerhalb $N$ kennzeichnen.
\subsection{Längstes gemeinsames Präfix}
In der Textindizierung können RMQs zum Auffinden von LCPs (Longest Common Prefix) bei der Mustersuche verwendet werden (bzw. Longest Common Suffix durch RMQ auf den umgedrehten Text), wobei $\operatorname{LCP}_T(i, j)$ das LCP eines gemeinsamen Präfixes der Suffixe berechnet, die in $T$ an Position $i$ und $j$ beginnen. \par Hierzu wird das LCP-Array $H$ des Suffix-Arrays\footnote{Das Suffix-Array $A$ enthält lexikografisch aufeinander-folgende Suffixe und $H[i] = \operatorname{LCP}(A[i], A[i-1])$.} $A$ verwendet und zu $T$ ein \emph{inverses Suffix-Array} $A^{-1}$ berechnet, welches zum $i$-ten Suffix im Suffixarray dessen Anfangsposition in $T$ liefert. Auf Basis dieser beiden Strukturen kann nun in konstanter Zeit die Länge des LCP mittels foldenger Vorschrift berechnet werden: $\operatorname{LCP}(i, j) = \operatorname{RMQ}_H(A^{-1}[i]+1, A^{-1}[j])$. \citep[S.43-43]{fischer2006theoretical}\bigskip\\
Für weitere Anwendungsfälle von RMQs sei auf \citet[S.3]{fischer2007new} verwiesen.

\bibliographystyle{plainnat}
\bibliography{Literature/litrerature}
\end{document}
